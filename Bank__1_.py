class Customer:
    def __init__(self, name, balance):
        self.name = name
        self.balance = balance
        
    def withdraw(self, fund):
        if fund > self.balance:
            print('failed to withdraw. balance not sufficient')
            return None
        else:
            self.balance -= fund
            print(f"{self.name} withdrawing: {fund}")
            return self.balance
        
    def deposit(self, fund):
        self.balance += fund
        
    def transfer(self, _to, fund):
        to_bal = _to.get_balance()
        _ = self.withdraw(fund)
        if _ == None:
            print('transaction failed')
        else:
            _to.deposit(fund)
            print(f"{self.name} depositing {fund} into {_to.name}")
            
        
    def get_balance(self):
        return self.balance
    
    def __repr__(self):
        return f"{self.balance}"
    
import threading

A = Customer('A', 700)
B = Customer('B', 800)
C = Customer('C', 400)

def transactions():
    print('Before customer transactions')
    for x in [A, B, C]:
        print(f"Account {x} balance: {x.get_balance()}")
    A.transfer(B, 200)
    A.transfer(C, 100)
    B.transfer(C, 100)
    C.withdraw(25)
    print('After customer transactions')
    for x in [A, B, C]:
        print(f"Account {x.name} balance: {x.get_balance()}")
    return

threads = []
for i in range(1):
    t = threading.Thread(target=transactions)
    threads.append(t)
    t.start()